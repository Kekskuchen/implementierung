package gui;
/**
 * Sample Skeleton for 'Hauptfenster.fxml' Controller Class
 */

import java.net.URL;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import helpfulThings.StringFormulaConverter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import net.sf.tweety.commons.Answer;
import net.sf.tweety.logics.cl.syntax.Conditional;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalFormula;
import scala.util.Left;
import wcl.CombinedBeliefSet;
import wcl.RankingFunktionWithWeakConditionals;
import wcl.WeakConditional;
import wcl.ZWClReasoner;

public class HauptfensterController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="vBoxConditionals"
    private VBox vBoxConditionals; // Value injected by FXMLLoader

    @FXML // fx:id="hBoxConditional1"
    private HBox hBoxConditional1; // Value injected by FXMLLoader

    @FXML // fx:id="conclusionConditional1"
    private TextField conclusionConditional1; // Value injected by FXMLLoader

    @FXML // fx:id="premiseConditional1"
    private TextField premiseConditional1; // Value injected by FXMLLoader

    @FXML // fx:id="deleteConditionalButton1"
    private Button deleteConditionalButton1; // Value injected by FXMLLoader

    @FXML // fx:id="hBoxConditional2"
    private HBox hBoxConditional2; // Value injected by FXMLLoader

    @FXML // fx:id="conclusionConditional2"
    private TextField conclusionConditional2; // Value injected by FXMLLoader

    @FXML // fx:id="premiseConditional2"
    private TextField premiseConditional2; // Value injected by FXMLLoader

    @FXML // fx:id="deleteConditionalButton2"
    private Button deleteConditionalButton2; // Value injected by FXMLLoader

    @FXML // fx:id="hBoxConditional3"
    private HBox hBoxConditional3; // Value injected by FXMLLoader

    @FXML // fx:id="conclusionConditional3"
    private TextField conclusionConditional3; // Value injected by FXMLLoader

    @FXML // fx:id="premiseConditional3"
    private TextField premiseConditional3; // Value injected by FXMLLoader

    @FXML // fx:id="deleteConditionalButton3"
    private Button deleteConditionalButton3; // Value injected by FXMLLoader

    @FXML // fx:id="addConditionalButton"
    private Button addConditionalButton; // Value injected by FXMLLoader

    @FXML // fx:id="vBoxWeakConditionals"
    private VBox vBoxWeakConditionals; // Value injected by FXMLLoader

    @FXML // fx:id="hBoxWeakConditional1"
    private HBox hBoxWeakConditional1; // Value injected by FXMLLoader

    @FXML // fx:id="conclusionWeakConditional1"
    private TextField conclusionWeakConditional1; // Value injected by FXMLLoader

    @FXML // fx:id="premiseWeakConditional1"
    private TextField premiseWeakConditional1; // Value injected by FXMLLoader

    @FXML // fx:id="deleteWeakConditional1Button"
    private Button deleteWeakConditional1Button; // Value injected by FXMLLoader

    @FXML // fx:id="hboxWeakConditional2"
    private HBox hboxWeakConditional2; // Value injected by FXMLLoader

    @FXML // fx:id="conclusionWeakConditional2"
    private TextField conclusionWeakConditional2; // Value injected by FXMLLoader

    @FXML // fx:id="premiseWeakConditional2"
    private TextField premiseWeakConditional2; // Value injected by FXMLLoader

    @FXML // fx:id="deleteWeakConditional2Button"
    private Button deleteWeakConditional2Button; // Value injected by FXMLLoader

    @FXML // fx:id="hBoxWeakConditional3"
    private HBox hBoxWeakConditional3; // Value injected by FXMLLoader

    @FXML // fx:id="conclusionWeakConditional3"
    private TextField conclusionWeakConditional3; // Value injected by FXMLLoader

    @FXML // fx:id="premiseWeakConditional3"
    private TextField premiseWeakConditional3; // Value injected by FXMLLoader

    @FXML // fx:id="deleteWeakConditional3Button"
    private Button deleteWeakConditional3Button; // Value injected by FXMLLoader

    @FXML // fx:id="newWeakConditionalButton"
    private Button newWeakConditionalButton; // Value injected by FXMLLoader

    @FXML // fx:id="questionTextArea"
    private TextField questionTextArea; // Value injected by FXMLLoader

    @FXML // fx:id="questionButton"
    private Button questionButton; // Value injected by FXMLLoader

    @FXML // fx:id="answerTextArea"
    private TextArea answerTextArea; // Value injected by FXMLLoader

    @FXML
    private ChoiceBox<String> questionChoiceBox;

    @FXML
    void addConditional(ActionEvent event) {
    	System.out.println("huhu ich f�ge ein Konditional hinzu");
    	ObservableList<Node>children =vBoxConditionals.getChildren();
    	HBox hbox = new HBox();
    	hbox.setAlignment(Pos.CENTER_LEFT);
    	hbox.setPadding(new Insets(0, 5, 0, 5));
    	Region region = new Region();
    	region.setPrefWidth(10);
    	//hbox.setSpacing(5);
        Text label1 = new Text("( ");
        Text label2 = new Text(" | ");
        Text label3 = new Text(" )");
        Pane pane = new Pane();
        TextField premise = new TextField();
        premise.setPrefWidth(100);
        HBox.setHgrow(premise, Priority.ALWAYS);
        TextField conclusion = new TextField();
        conclusion.setPrefWidth(100);
        HBox.setHgrow(conclusion, Priority.ALWAYS);
        Button button = new Button("-");
        hbox.getChildren().addAll(label1, conclusion, label2, premise, label3, region, button);
        HBox.setHgrow(pane, Priority.ALWAYS);
        children.add(hbox);
            button.setOnAction(e -> vBoxConditionals.getChildren().remove(hbox));
            this.initialize();
            System.out.println("huhu ich habs geschafft");
    }

    @FXML
    void computeCRepresentation(ActionEvent event) {
    	CombinedBeliefSet bel = new CombinedBeliefSet();

    	for (Node node : vBoxConditionals.getChildren()) {
			if(node instanceof HBox){
				Node c = ((HBox) node).getChildren().get(1);
				String conclusion = "";
				if(c instanceof TextField)
					conclusion = ((TextField) c).getText();

			    c = ((HBox) node).getChildren().get(3);
				String premise = "";
				if(c instanceof TextField)
					premise = ((TextField) c).getText();
				if(!conclusion.equals("") && !premise.equals("")){
					try{
						bel.addPositiveConditional(new Conditional(StringFormulaConverter.convertStringToFormula(premise), StringFormulaConverter.convertStringToFormula(conclusion)));
					}
					catch(IllegalArgumentException e){
						answerTextArea.setText("Konditional nicht richtig angegeben");
					}
				}
				else if(!conclusion.equals("")){
					try{
					bel.addPositiveConditional(new Conditional(StringFormulaConverter.convertStringToFormula(conclusion)));

					}
					catch(IllegalArgumentException e){
						answerTextArea.setText("Konditional nicht richtig angegeben");
					}
				}

			}
		}

    	for (Node node : vBoxWeakConditionals.getChildren()) {
			if(node instanceof HBox){
				Node c = ((HBox) node).getChildren().get(1);
				String conclusion = "";
				if(c instanceof TextField)
					conclusion = ((TextField) c).getText();

			    c = ((HBox) node).getChildren().get(3);
				String premise = "";
				if(c instanceof TextField)
					premise = ((TextField) c).getText();
				if(!conclusion.equals("") && !premise.equals("")){
					try{
						bel.addNegativeConditional(new WeakConditional(StringFormulaConverter.convertStringToFormula(premise), StringFormulaConverter.convertStringToFormula(conclusion)));
					}
					catch(IllegalArgumentException e){
						answerTextArea.setText("Konditional nicht richtig angegeben");
					}
				}

			}
		}
    	if(!bel.equals(new CombinedBeliefSet())){
    	ZWClReasoner reasoner = new ZWClReasoner(bel);
    	RankingFunktionWithWeakConditionals function = reasoner.computeCRepresentation();
    	answerTextArea.setText("Wissensbasis: \n" + bel.toString() + "\n Z-C-Repr�sentation zu K: \n" + function.toString());
    	}
    	else{
    		answerTextArea.setText("Wissensbasis: \n" + bel.toString());
    	}
    	}

    @FXML
    void deleteConditional1OnAction(ActionEvent event) {
    	vBoxConditionals.getChildren().remove(hBoxConditional3);

    }

    @FXML
    void deleteConditional2OnAction(ActionEvent event) {
    	vBoxConditionals.getChildren().remove(hBoxConditional2);
    }

    @FXML
    void deleteConditional3OnAction(ActionEvent event) {
    	vBoxConditionals.getChildren().remove(hBoxConditional3);
    }

    @FXML
    void deleteWeakConditional1(ActionEvent event) {
    	vBoxWeakConditionals.getChildren().remove(hBoxWeakConditional1);
    }

    @FXML
    void deleteWeakConditional2(ActionEvent event) {
    	vBoxWeakConditionals.getChildren().remove(hboxWeakConditional2);
    }

    @FXML
    void deleteWeakConditional3(ActionEvent event) {
    	vBoxWeakConditionals.getChildren().remove(hBoxWeakConditional3);
    }

    @FXML
    void newWeakConditional(ActionEvent event) {
    	System.out.println("huhu ich f�ge ein Konditional hinzu");
    	ObservableList<Node>children =vBoxWeakConditionals.getChildren();
    	HBox hbox = new HBox();
    	hbox.setAlignment(Pos.CENTER_LEFT);
    	hbox.setPadding(new Insets(0, 5, 0, 5));
    	Region region = new Region();
    	region.setPrefWidth(10);
    	//hbox.setSpacing(5);
        Text label1 = new Text("( | ");
        Text label2 = new Text(" | ");
        Text label3 = new Text(" | )");
        Pane pane = new Pane();
        TextField premise = new TextField();
        premise.setPrefWidth(100);
        HBox.setHgrow(premise, Priority.ALWAYS);
        TextField conclusion = new TextField();
        conclusion.setPrefWidth(100);
        HBox.setHgrow(conclusion, Priority.ALWAYS);
        Button button = new Button("-");
        hbox.getChildren().addAll(label1, conclusion, label2, premise, label3, region, button);
        HBox.setHgrow(pane, Priority.ALWAYS);
        children.add(hbox);
            button.setOnAction(e -> vBoxWeakConditionals.getChildren().remove(hbox));
            this.initialize();
            System.out.println("huhu ich habs geschafft");
    }

    @FXML
    void questionButtonPressed(ActionEvent event) {

    	CombinedBeliefSet bel = new CombinedBeliefSet();

    	for (Node node : vBoxConditionals.getChildren()) {
			if(node instanceof HBox){
				Node c = ((HBox) node).getChildren().get(1);
				String conclusion = "";
				if(c instanceof TextField)
					conclusion = ((TextField) c).getText();

			    c = ((HBox) node).getChildren().get(3);
				String premise = "";
				if(c instanceof TextField)
					premise = ((TextField) c).getText();
				if(!conclusion.equals("") && !premise.equals("")){
					try{
						bel.addPositiveConditional(new Conditional(StringFormulaConverter.convertStringToFormula(premise), StringFormulaConverter.convertStringToFormula(conclusion)));
					}
					catch(IllegalArgumentException e){
						answerTextArea.setText("Konditional nicht richtig angegeben");
					}
				}
				else if(!conclusion.equals("")){
					try{
					bel.addPositiveConditional(new Conditional(StringFormulaConverter.convertStringToFormula(conclusion)));

					}
					catch(IllegalArgumentException e){
						answerTextArea.setText("Konditional nicht richtig angegeben");
					}
				}

			}
		}

    	for (Node node : vBoxWeakConditionals.getChildren()) {
			if(node instanceof HBox){
				Node c = ((HBox) node).getChildren().get(1);
				String conclusion = "";
				if(c instanceof TextField)
					conclusion = ((TextField) c).getText();

			    c = ((HBox) node).getChildren().get(3);
				String premise = "";
				if(c instanceof TextField)
					premise = ((TextField) c).getText();
				if(!conclusion.equals("") && !premise.equals("")){
					try{
						bel.addNegativeConditional(new WeakConditional(StringFormulaConverter.convertStringToFormula(premise), StringFormulaConverter.convertStringToFormula(conclusion)));
					}
					catch(IllegalArgumentException e){
						answerTextArea.setText("Konditional nicht richtig angegeben");
					}
				}

			}
		}

    	answerTextArea.setText("Wissensbasis: \n" + bel.toString());


    	ZWClReasoner reasoner = new ZWClReasoner(bel);

    	String text = questionTextArea.getText();

    	if(!text.isEmpty()){
    		String choice = questionChoiceBox.getValue();
    		Answer answer;
    		if(choice.equals("Konditional")){
    			try{
    				Conditional cond = StringFormulaConverter.convertStringToConditional(text);
    				answer = reasoner.query(cond);
    				answerTextArea.setText(answerTextArea.getText() + "\n" + answer.getText());
    			}
    			catch(IllegalArgumentException e){
    				answerTextArea.setText("Eingabe ist kein Syntaktisch korrektes Konditional");
    			}
    		}
    		else if(choice.equals("negatives Konditional")){
    			try{
    				WeakConditional cond = StringFormulaConverter.convertStringToWeakConditional(text);
    				answer = reasoner.query(cond);
    				answerTextArea.setText(answerTextArea.getText() + "\n" + answer.getText());
    			}
    			catch(IllegalArgumentException e){
    				answerTextArea.setText("Eingabe ist kein Syntaktisch korrektes schwaches Konditional");
    			}
    		}
    		else{
    			try{
    				PropositionalFormula prop = StringFormulaConverter.convertStringToFormula(text);
    				answer = reasoner.query(prop);
    				answerTextArea.setText(answerTextArea.getText() + "\n" + answer.getText());
    			}
    			catch(IllegalArgumentException e){
    				answerTextArea.setText("Eingabe ist keine Syntaktisch Korrekte Aussagenlogische Formel");
    			}
    		}

    	}

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert vBoxConditionals != null : "fx:id=\"vBoxConditionals\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert hBoxConditional1 != null : "fx:id=\"hBoxConditional1\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert conclusionConditional1 != null : "fx:id=\"conclusionConditional1\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert premiseConditional1 != null : "fx:id=\"premiseConditional1\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert deleteConditionalButton1 != null : "fx:id=\"deleteConditionalButton1\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert hBoxConditional2 != null : "fx:id=\"hBoxConditional2\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert conclusionConditional2 != null : "fx:id=\"conclusionConditional2\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert premiseConditional2 != null : "fx:id=\"premiseConditional2\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert deleteConditionalButton2 != null : "fx:id=\"deleteConditionalButton2\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert hBoxConditional3 != null : "fx:id=\"hBoxConditional3\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert conclusionConditional3 != null : "fx:id=\"conclusionConditional3\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert premiseConditional3 != null : "fx:id=\"premiseConditional3\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert deleteConditionalButton3 != null : "fx:id=\"deleteConditionalButton3\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert addConditionalButton != null : "fx:id=\"addConditionalButton\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert vBoxWeakConditionals != null : "fx:id=\"vBoxWeakConditionals\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert hBoxWeakConditional1 != null : "fx:id=\"hBoxWeakConditional1\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert conclusionWeakConditional1 != null : "fx:id=\"conclusionWeakConditional1\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert premiseWeakConditional1 != null : "fx:id=\"premiseWeakConditional1\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert deleteWeakConditional1Button != null : "fx:id=\"deleteWeakConditional1Button\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert hboxWeakConditional2 != null : "fx:id=\"hboxWeakConditional2\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert conclusionWeakConditional2 != null : "fx:id=\"conclusionWeakConditional2\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert premiseWeakConditional2 != null : "fx:id=\"premiseWeakConditional2\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert deleteWeakConditional2Button != null : "fx:id=\"deleteWeakConditional2Button\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert hBoxWeakConditional3 != null : "fx:id=\"hBoxWeakConditional3\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert conclusionWeakConditional3 != null : "fx:id=\"conclusionWeakConditional3\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert premiseWeakConditional3 != null : "fx:id=\"premiseWeakConditional3\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert deleteWeakConditional3Button != null : "fx:id=\"deleteWeakConditional3Button\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert newWeakConditionalButton != null : "fx:id=\"newWeakConditionalButton\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert questionTextArea != null : "fx:id=\"questionTextArea\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        assert questionButton != null : "fx:id=\"questionButton\" was not injected: check your FXML file 'Hauptfenster.fxml'.";
        questionChoiceBox.setItems(FXCollections.observableArrayList("Aussage", "Konditional", "negatives Konditional"));
        questionChoiceBox.setValue("Aussage");
        answerTextArea.setText("Aussagenlogische Formeln m�ssen in KNF und ohne Klammerung angegeben werden");

    }

}


