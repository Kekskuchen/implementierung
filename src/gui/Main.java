package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application{

	Stage mystage;
	@Override
    public void start(Stage primaryStage) throws Exception {
    	BorderPane root = new BorderPane();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Hauptfenster.fxml"));
		try {
			root = fxmlLoader.load();
			Scene scene = new Scene(root);
			primaryStage.setTitle("C-Repräsentationen mit positiven und negativen Konditionalen");
			primaryStage.setScene(scene);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		mystage = primaryStage;

		primaryStage.show();
    }

	public void update(){
		mystage.show();
	}

    public static void main(String[] args) {
		launch(args);
	}

}
