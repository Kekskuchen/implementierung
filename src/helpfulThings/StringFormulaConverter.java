package helpfulThings;

import java.util.StringTokenizer;

import net.sf.tweety.logics.cl.syntax.Conditional;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalFormula;
import wcl.WeakConditional;

public class StringFormulaConverter {
	public static PropositionalFormula convertStringToFormula(String input){
		if(input== null || input.equals("")||input.contains("+")){
			throw new IllegalArgumentException();
		}
		String text = input.trim();
		if(input.contains("&&")){
		text = input.replaceAll("&&", "+");
		System.out.println(text);
		if(text.contains("&")){
			throw new IllegalArgumentException();
		}
		StringTokenizer tok = new StringTokenizer(text,"+");
		String str = tok.nextToken();
		if(str.isEmpty())
			throw new IllegalArgumentException();
		PropositionalFormula act = convertHelp1(str);
		while(tok.hasMoreTokens()){
			act = act.combineWithAnd(convertHelp1(tok.nextToken()));
		}

		return act;
		}
		return convertHelp1(text);
	}

	private static PropositionalFormula convertHelp1(String input){
		if(input== null || input.equals("")){
			throw new IllegalArgumentException();
		}
		System.out.println("hallo ich bin beim oder");
		if(input.contains("|")){
			System.out.println("hallo ich hab ein oder");

		StringTokenizer tok = new StringTokenizer(input,"|");
		String str = tok.nextToken();
			System.out.println(str);
		if(str.isEmpty())
			throw new IllegalArgumentException();
		PropositionalFormula act = convertHelp2(str);
		System.out.println(act.toString());

		while(tok.hasMoreTokens()){
			act =(PropositionalFormula) act.combineWithOr((convertHelp2(tok.nextToken())));
			System.out.println(act.toString());

		}

		return act;
		}
		return convertHelp2(input);
	}

	private static PropositionalFormula convertHelp2(String input){
		if(input== null || input.equals("")||input.contains("+")|| input.equals("!")){
			throw new IllegalArgumentException();
		}
		if(input.startsWith("!")){
			return(PropositionalFormula) new Proposition(input.substring(1)).complement();
		}
		return new Proposition(input);
	}

	public static Conditional convertStringToConditional(String input){
			input = input.trim();
			if(!input.startsWith("(") || !input.endsWith(")"))
					throw new IllegalArgumentException();
			String text = input.substring(1, input.length()-1);
			text = text.replace('|', '-');
			System.out.println(text);
		text = text.replaceAll("--", "|");
			StringTokenizer tok = new StringTokenizer(text, "-");
			if(tok.countTokens() != 2)
				throw new IllegalArgumentException();
			if(tok.countTokens()==2){
			PropositionalFormula conclusion = StringFormulaConverter.convertStringToFormula(tok.nextToken());
			PropositionalFormula premise = StringFormulaConverter.convertStringToFormula(tok.nextToken());
			return new Conditional(premise, conclusion);
			}
			return null;
	}

	public static WeakConditional convertStringToWeakConditional(String input){
		input = input.trim();
		if(!input.startsWith("(|") || !input.endsWith("|)"))
				throw new IllegalArgumentException();
		String text = input.substring(2, input.length()-2);
		text = text.replace('|', '-');
		System.out.println(text);
	text = text.replaceAll("--", "|");
		StringTokenizer tok = new StringTokenizer(text, "-");
		if(tok.countTokens() != 2)
			throw new IllegalArgumentException();
		PropositionalFormula conclusion = StringFormulaConverter.convertStringToFormula(tok.nextToken());
		PropositionalFormula premise = StringFormulaConverter.convertStringToFormula(tok.nextToken());
		return new WeakConditional(premise, conclusion);
}
}
