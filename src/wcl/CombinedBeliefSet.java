package wcl;

import java.util.Collection;
import java.util.Iterator;

import net.sf.tweety.logics.cl.ClBeliefSet;
import net.sf.tweety.logics.cl.syntax.Conditional;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;

public class CombinedBeliefSet {

	private ClBeliefSet positiveConditionals;
	private WclBeliefSet weakConditionals;

	/**
	 * creates empty beliefset
	 */
	public CombinedBeliefSet(){
		positiveConditionals = new ClBeliefSet();
		weakConditionals = new WclBeliefSet();
	}

	/**
	 * Creates a new belief set with the given collection of
	 * formulae.
	 * @param conditionals a collection of Conditionals.
	 * @param weakconditionals a collection of negative Conditionals
	 */
	public CombinedBeliefSet(Collection<? extends Conditional> conditionals, Collection<? extends WeakConditional> weakconditionals){
		positiveConditionals = new ClBeliefSet(conditionals);
		weakConditionals = new WclBeliefSet(weakconditionals);
	}

	/**
	 * Creates a new belief set with the given belief
	 * set.
	 * @param positiveBeliefSet belief set of positive Conditionals
	 * @param negativeBeliefSet belief set of negative Conditionals
	 */
	public CombinedBeliefSet(ClBeliefSet positiveBeliefSet, WclBeliefSet negativeBeliefSet){
		positiveConditionals = positiveBeliefSet;
		weakConditionals = negativeBeliefSet;
	}

	/**
	 * returns the positive BeliefSet
	 * @return positive BeliefSet
	 */
	public ClBeliefSet getPositiveBeliefSet(){
		return positiveConditionals;
	}

	/**
	 * returns the negative BeliefSet
	 * @return negative BeliefSet
	 */
	public WclBeliefSet getNegativeBeliefSet(){
		return weakConditionals;
	}

	/**
	 * sets the positive Beliefset
	 * @param positiveBeliefset the new positive Beliefset
	 */
	public void setPositiveBeliefSet(ClBeliefSet positiveBeliefset){
		if(positiveBeliefset == null)
			throw new IllegalArgumentException("beliefset has to exist");
		this.positiveConditionals = positiveBeliefset;
	}

	/**
	 * sets the negative Beliefset
	 * @param negativeBeliefset the new negative Beliefset
	 */
	public void setNegativeBeliefSet(WclBeliefSet negativeBeliefset){
		if(negativeBeliefset == null)
			throw new IllegalArgumentException("beliefset has to exist");
		this.weakConditionals= negativeBeliefset;
	}

	public void add(CombinedBeliefSet set){
		positiveConditionals.addAll(set.getPositiveBeliefSet());
		weakConditionals.addAll(set.getNegativeBeliefSet());
	}

	/**
	 * adds a single Conditional to the positive Beliefset
	 * @param conditional conditional that should be added
	 */
	public void addPositiveConditional(Conditional conditional){
		positiveConditionals.add(conditional);
	}

	/**
	 * adds a single negative Conditional to the negative Beliefset
	 * @param conditional weak conditional that should be added
	 */
	public void addNegativeConditional(WeakConditional conditional){
		weakConditionals.add(conditional);
	}

	/**
	 * adds a set of positive conditionals to the positive Beliefset
	 * @param conditionals set of conditionals that should be added
	 */
	public void addPositiveConditionals(Collection<? extends Conditional> conditionals){
		positiveConditionals.addAll(conditionals);
	}

	/**
	 * adds a set of negative conditionals to the negative Beliefset
	 * @param conditionals set of  weak conditionals that should be added
	 */
	public void addNegativeConditionals(Collection<? extends WeakConditional> conditionals){
		weakConditionals.addAll(conditionals);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CombinedBeliefSet other = (CombinedBeliefSet) obj;
		return positiveConditionals.equals(other.getPositiveBeliefSet()) && weakConditionals.equals(other.getNegativeBeliefSet());
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#isEmpty()
	 */
	public boolean isEmpty()
	{
		return positiveConditionals.isEmpty() && weakConditionals.isEmpty();
	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.kr.BeliefBase#getSignature()
	 */
	public PropositionalSignature getSignature(){
		PropositionalSignature prop = positiveConditionals.getSignature();
		prop.add(weakConditionals.getSignature());
		return prop;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#clear()
	 */
	public void clear(){
		positiveConditionals = new ClBeliefSet();
		weakConditionals = new WclBeliefSet();
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#contains(java.lang.Object)
	 */
	public boolean contains(Object o){
		return positiveConditionals.contains(o) || weakConditionals.contains(o);
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#containsAll(java.util.Collection)
	 */
	public boolean containsAll(Collection<?> c){
		for(Object o:c){
			if(!positiveConditionals.contains(o) && !weakConditionals.contains(o))
				return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#remove(java.lang.Object)
	 */
	public boolean remove(Object o){
		if(positiveConditionals.contains(o)){
			return positiveConditionals.remove(o);
		}
		else {
			return weakConditionals.remove(o);
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#removeAll(java.util.Collection)
	 */
	public boolean removeAll(Collection<?> c){
		boolean result = true;
		for(Object t: c){
			boolean sub = this.remove(t);
			result = result && sub;
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#size()
	 */
	public int size(){
		return positiveConditionals.size() + weakConditionals.size();
	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.kr.BeliefBase#toString()
	 */
	public String toString(){
		String s = "K = Positive Konditionale: { ";
		Iterator<Conditional> it = positiveConditionals.iterator();
		if(it.hasNext())
			s += it.next();
		while(it.hasNext())
			s += ", " + it.next();
		s += " },\n       Negative Kondditionale: {";
		Iterator<WeakConditional> weaks = weakConditionals.iterator();
		if(weaks.hasNext())
			s += weaks.next();
		while(weaks.hasNext())
			s += ", " + weaks.next();
		s += " }";
		return s;
	}

	public CombinedBeliefSet clone(){
		ClBeliefSet poscopy = new ClBeliefSet();
		Iterator<Conditional> i = positiveConditionals.iterator();
		while(i.hasNext()){
			poscopy.add(i.next());
		}
		WclBeliefSet negcopy = new WclBeliefSet();
		Iterator<WeakConditional> j = weakConditionals.iterator();
		while(j.hasNext()){
			negcopy.add(j.next());
		}
		return new CombinedBeliefSet(poscopy, negcopy);
	}
}
