package wcl;

import java.util.HashMap;
import java.util.Map;

import net.sf.tweety.commons.BeliefBase;
import net.sf.tweety.commons.BeliefSet;
import net.sf.tweety.commons.Formula;
import net.sf.tweety.logics.cl.ClBeliefSet;
import net.sf.tweety.logics.cl.semantics.RankingFunction;
import net.sf.tweety.logics.cl.syntax.Conditional;
import net.sf.tweety.logics.commons.syntax.interfaces.SimpleLogicalFormula;
import net.sf.tweety.logics.pl.semantics.PossibleWorld;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;

public class RankingFunktionWithWeakConditionals extends RankingFunction {

	/**
	 * The ranks of the possible worlds.
	 */


	public RankingFunktionWithWeakConditionals(PropositionalSignature signature){
		super(signature);

	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.logic.Interpretation#satisfies(net.sf.tweety.logic.Formula)
	 */
	@Override
	public boolean satisfies(Formula formula) throws IllegalArgumentException{
		if(!(formula instanceof Conditional)&& ! (formula instanceof WeakConditional))
			throw new IllegalArgumentException("Formula " + formula + " is not a conditional expression.");
		if(formula instanceof Conditional){
			Conditional c = (Conditional) formula;
			Integer rankPremiseAndConclusion = this.rank(c.getConclusion().combineWithAnd(c.getPremise().iterator().next()));
			System.out.println("Konditional: " + c.toString() +", VerifiziertRang: " + rankPremiseAndConclusion);
			Integer rankPremiseAndNotConclusion = this.rank(c.getConclusion().complement().combineWithAnd(c.getPremise().iterator().next()));
			System.out.println("Konditional: " + c.toString() +", FalsifiziertRang: " + rankPremiseAndNotConclusion);
			return rankPremiseAndConclusion < rankPremiseAndNotConclusion;
		}
		WeakConditional c = (WeakConditional) formula;
		Integer rankPremiseAndConclusion = this.rank(c.getConclusion().combineWithAnd(c.getPremise()));
		System.out.println("Konditional: " + c.toString() +", VerifiziertRang: " + rankPremiseAndConclusion);
		Integer rankPremiseAndNotConclusion = this.rank(c.getConclusion().complement().combineWithAnd(c.getPremise()));
		System.out.println("Konditional: " + c.toString() +", FalsifiziertRang: " + rankPremiseAndNotConclusion);
		return rankPremiseAndConclusion <= rankPremiseAndNotConclusion;
	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.kr.Interpretation#satisfies(net.sf.tweety.logic.KnowledgeBase)
	 */
	public boolean satisfies(ClBeliefSet beliefBase){
		System.out.println("Satisfies pos?");
		System.out.println(beliefBase.toString());
		for(Formula f: ((ClBeliefSet)beliefBase))
			if(!this.satisfies(f))
				return false;
		return true;

	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.kr.Interpretation#satisfies(net.sf.tweety.logic.KnowledgeBase)
	 */
	public boolean satisfies(WclBeliefSet beliefBase){
		System.out.println("Satisfies neg?");
		System.out.println(beliefBase.toString());
		for(Formula f: ((WclBeliefSet)beliefBase))
			if(!this.satisfies(f))
				return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.kr.Interpretation#satisfies(net.sf.tweety.logic.KnowledgeBase)
	 */
	public boolean satisfies(CombinedBeliefSet beliefBase){
		System.out.println("satisfies?");
		System.out.println(beliefBase.toString());
		return satisfies(beliefBase.getPositiveBeliefSet()) && satisfies((WclBeliefSet)beliefBase.getNegativeBeliefSet());
	}

	/**
	 * Checks whether the given possible world w verifies the given
	 * weak conditional (|B|A|), i.e. whether w not satisfies A and not B
	 * @param w a possible world
	 * @param c a weak conditional.
	 * @return "true" if the given possible world verifies the given weak conditional.
	 */
	public static boolean verifies(PossibleWorld w, WeakConditional c){
		SimpleLogicalFormula formula = c.getPremise().combineWithAnd(c.getConclusion());
		return w.satisfies(formula);
	}


	/**
	 * Checks whether the given possible world w falsifies the given
	 * weak conditional (|B|A|), i.e. whether w satisfies A and not B
	 * @param w a possible world
	 * @param c a weak conditional.
	 * @return "true" if the given possible world falsifies the given weak conditional.
	 */
	public static boolean falsifies(PossibleWorld w, WeakConditional c){
		SimpleLogicalFormula formula = c.getPremise().combineWithAnd(c.getConclusion().complement());
		return w.satisfies(formula);
	}

	/**
	 * Checks whether the given possible world w satisfies the given
	 * weak conditional (|B|A|), i.e. whether w does not falsify c.
	 * @param w a possible world
	 * @param c a weak conditional.
	 * @return "true" if the given possible world satisfies the given weak conditional.
	 */
	public static boolean satisfies(PossibleWorld w, WeakConditional c){
		return !RankingFunktionWithWeakConditionals.falsifies(w, c);
	}

	@Override
	public boolean equals(Object other) {
		if(! (other instanceof RankingFunktionWithWeakConditionals))
			return false;
		System.out.println("hallo");
		RankingFunktionWithWeakConditionals oc = (RankingFunktionWithWeakConditionals)other;
		if(oc == null)
			System.out.println("oc ist null");
		if(!this.getSignature().equals(oc.getSignature()))
			return false;
		for (PossibleWorld w : this.getPossibleWorlds()) {
			if(this.rank(w) != oc.rank(w))
					return false;
		}
		return true;
	}
}
