package wcl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.sf.tweety.commons.Answer;
import net.sf.tweety.commons.Formula;
import net.sf.tweety.commons.Reasoner;
import net.sf.tweety.logics.cl.ClBeliefSet;
import net.sf.tweety.logics.cl.semantics.RankingFunction;
import net.sf.tweety.logics.cl.syntax.Conditional;
import net.sf.tweety.logics.pl.semantics.PossibleWorld;
import net.sf.tweety.logics.pl.syntax.PropositionalFormula;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;
import net.sf.tweety.math.GeneralMathException;
import net.sf.tweety.math.equation.Equation;
import net.sf.tweety.math.equation.Inequation;
import net.sf.tweety.math.equation.Statement;
import net.sf.tweety.math.opt.OptimizationProblem;
import net.sf.tweety.math.opt.Solver;
import net.sf.tweety.math.term.IntegerConstant;
import net.sf.tweety.math.term.IntegerVariable;
import net.sf.tweety.math.term.Term;
import net.sf.tweety.math.term.Variable;

public class WClReasoner extends Reasoner{

	private CombinedBeliefSet mybeliefbase;
	private RankingFunktionWithWeakConditionals crepresentation;
	private Map<Conditional, Integer> posPartitionranks;
	private Map<WeakConditional, Integer> negPartitionranks;

	public WClReasoner(CombinedBeliefSet beliefbase){
		super(beliefbase.getPositiveBeliefSet());
		this.mybeliefbase = beliefbase;

	}

	@Override
	public Answer query(Formula query) {
		if(!(query instanceof Conditional) && !(query instanceof PropositionalFormula) && !(query instanceof WeakConditional))
			throw new IllegalArgumentException("Reasoning in conditional logic is only defined for conditional and propositional queries.");
		RankingFunktionWithWeakConditionals crepresentation = this.getCRepresentation();
		if(query instanceof Conditional || query instanceof WeakConditional){
			Answer answer = new Answer(this.getKnowledgeBase(),query);
			boolean bAnswer = crepresentation.satisfies(query);
			answer.setAnswer(bAnswer);
			answer.appendText("The answer is: " + bAnswer);
			return answer;
		}
		if(query instanceof PropositionalFormula){
			int rank = crepresentation.rank(query);
			Answer answer = new Answer(this.getKnowledgeBase(),query);
			answer.setAnswer(rank==0);
			answer.appendText("The rank of the query is " + rank + " (the query is " + ((rank==0)?(""):("not ")) + "believed)");
			return answer;
		}

		return null;
	}

	private RankingFunktionWithWeakConditionals getCRepresentation() {
		if(this.crepresentation == null)
			this.crepresentation = this.computeCRepresentation();
		return this.crepresentation;
	}

	private RankingFunktionWithWeakConditionals computeCRepresentation(){
		RankingFunktionWithWeakConditionals crep = new RankingFunktionWithWeakConditionals((PropositionalSignature)this.mybeliefbase.getSignature());
		ClBeliefSet kb = (ClBeliefSet) this.getKnowledgeBase();
		WclBeliefSet wkb = this.getNegativeKnowledgebase();
		Set<PossibleWorld> possibleWorlds = crep.getPossibleWorlds();
		// variables for ranks
		Map<PossibleWorld,IntegerVariable> ranks = new HashMap<PossibleWorld,IntegerVariable>();
		int i = 0;
		for(PossibleWorld w: possibleWorlds){
			ranks.put(w, new IntegerVariable("i" + i));
			i++;
		}
		// variables for kappas
		Map<Conditional,IntegerVariable> kappa_pos = new HashMap<Conditional,IntegerVariable>();
		Map<WeakConditional, IntegerVariable> weak_kappa_pos = new HashMap<>();
		Map<Conditional,IntegerVariable> kappa_neg = new HashMap<Conditional,IntegerVariable>();
		Map<WeakConditional, IntegerVariable> weak_kappa_neg = new HashMap<>();
		i = 1;
		for(Formula f: kb){
			kappa_pos.put((Conditional)f, new IntegerVariable("kp"+i));
			kappa_neg.put((Conditional)f, new IntegerVariable("km"+i));
			i++;
		}
		for(Formula f: wkb){
			weak_kappa_pos.put((WeakConditional)f, new IntegerVariable("kp"+i));
			weak_kappa_neg.put((WeakConditional)f, new IntegerVariable("km"+i));
			i++;
		}
		// represent optimization problem
		OptimizationProblem problem = new OptimizationProblem(OptimizationProblem.MINIMIZE);
		net.sf.tweety.math.term.Term targetFunction = null;
		for(IntegerVariable v: kappa_pos.values()){
			if(targetFunction == null)
				targetFunction = v;
			else targetFunction = v.add(targetFunction);
		}
		for(IntegerVariable v: weak_kappa_pos.values()){
			if(targetFunction == null)
				targetFunction = v;
			else targetFunction = v.add(targetFunction);
		}
		for(IntegerVariable v: kappa_neg.values()){
			if(targetFunction == null)
				targetFunction = v;
			else targetFunction = v.add(targetFunction);
		}
		for(IntegerVariable v: weak_kappa_neg.values()){
			if(targetFunction == null)
				targetFunction = v;
			else targetFunction = v.add(targetFunction);
		}

		problem.setTargetFunction(targetFunction);
		// for every conditional "cond" in "kb", "crep" should accept "cond"
		for(Formula f: kb)
			problem.add(this.getAcceptanceConstraint((Conditional)f, ranks));
		// for every weak conditional "cond" in "wkb", "crep" should accept "cond"
		for(Formula f: wkb)
			problem.add(this.getWeakAcceptanceConstraint((WeakConditional)f, ranks));
		// the ranking function should be indifferent to kb, i.e.
		// for every possible world "w" the rank of the world should obey the above constraint
		for(PossibleWorld w: ranks.keySet())
			problem.add(this.getRankConstraint(w, ranks.get(w), kappa_pos, kappa_neg, weak_kappa_pos, weak_kappa_neg));

		try {
			Map<Variable, Term> solution = Solver.getDefaultLinearSolver().solve(problem);
			// extract ranking function
			for(PossibleWorld w: ranks.keySet()){
				crep.setRank(w, ((IntegerConstant)solution.get(ranks.get(w))).getValue());
			}
			return crep;
		} catch (GeneralMathException e) {
			throw new RuntimeException(e);
		}
	}
	private Statement getAcceptanceConstraint(Conditional cond, Map<PossibleWorld,IntegerVariable> ranks){
		// construct left side of the inequation
		net.sf.tweety.math.term.Term leftSide = null;
		for(PossibleWorld w: ranks.keySet()){
			if(RankingFunction.verifies(w, cond)){
				if(leftSide == null)
					leftSide = ranks.get(w);
				else leftSide = leftSide.min(ranks.get(w));
			}
		}
		// if term is still null then set to constant zero
		if(leftSide == null)
			leftSide = new net.sf.tweety.math.term.IntegerConstant(0);
		// construct right side of the inequation
		net.sf.tweety.math.term.Term rightSide = null;
		for(PossibleWorld w: ranks.keySet()){
			if(RankingFunction.falsifies(w, cond)){
				if(rightSide == null)
					rightSide = ranks.get(w);
				else rightSide = rightSide.min(ranks.get(w));
			}
		}
		// if term is still null then set to constant zero
		if(rightSide == null)
			rightSide = new net.sf.tweety.math.term.IntegerConstant(0);
		// return inequation
		return new Inequation(leftSide.minus(rightSide),new IntegerConstant(0),Inequation.LESS);
	}

	private Statement getWeakAcceptanceConstraint(WeakConditional cond, Map<PossibleWorld,IntegerVariable> ranks){
		// construct left side of the inequation
		net.sf.tweety.math.term.Term leftSide = null;
		for(PossibleWorld w: ranks.keySet()){
			if(RankingFunktionWithWeakConditionals.verifies(w, cond)){
				if(leftSide == null)
					leftSide = ranks.get(w);
				else leftSide = leftSide.min(ranks.get(w));
			}
		}
		// if term is still null then set to constant zero
		if(leftSide == null)
			leftSide = new net.sf.tweety.math.term.IntegerConstant(0);
		// construct right side of the inequation
		net.sf.tweety.math.term.Term rightSide = null;
		for(PossibleWorld w: ranks.keySet()){
			if(RankingFunktionWithWeakConditionals.falsifies(w, cond)){
				if(rightSide == null)
					rightSide = ranks.get(w);
				else rightSide = rightSide.min(ranks.get(w));
			}
		}
		// if term is still null then set to constant zero
		if(rightSide == null)
			rightSide = new net.sf.tweety.math.term.IntegerConstant(0);
		// return inequation
		return new Inequation(leftSide.minus(rightSide),new IntegerConstant(0),Inequation.LESS_EQUAL);
	}

	private Statement getRankConstraint(PossibleWorld w, IntegerVariable ranki,
										Map<Conditional,IntegerVariable> kappa_pos, Map<Conditional,IntegerVariable> kappa_neg,
										Map<WeakConditional,IntegerVariable> weak_kappa_pos, Map<WeakConditional,IntegerVariable> weak_kappa_neg){
		// construct ride side of the inequation
		net.sf.tweety.math.term.Term rightSide = null;
		for(Conditional cond: kappa_pos.keySet()){
			if(RankingFunction.verifies(w, cond)){
				if(rightSide == null)
					rightSide = kappa_pos.get(cond);
				else
					rightSide = rightSide.add(kappa_pos.get(cond));
			}else if(RankingFunction.falsifies(w,cond)){
				if(rightSide == null)
					rightSide = kappa_neg.get(cond);
				else
					rightSide = rightSide.add(kappa_neg.get(cond));
			}
		}
		for(WeakConditional cond: weak_kappa_pos.keySet()){
			if(RankingFunktionWithWeakConditionals.verifies(w, cond)){
				if(rightSide == null)
					rightSide = weak_kappa_pos.get(cond);
				else
					rightSide = rightSide.add(weak_kappa_pos.get(cond));
			}else if(RankingFunktionWithWeakConditionals.falsifies(w,cond)){
				if(rightSide == null)
					rightSide = weak_kappa_neg.get(cond);
				else
					rightSide = rightSide.add(weak_kappa_neg.get(cond));
			}
		}
		// if term is still null then set to constant zero
		if(rightSide == null)
			rightSide = new net.sf.tweety.math.term.IntegerConstant(0);
		// return
		return new Equation(ranki.minus(rightSide),new IntegerConstant(0));
	}


	private WclBeliefSet getNegativeKnowledgebase(){
		return mybeliefbase.getNegativeBeliefSet();
	}



}
