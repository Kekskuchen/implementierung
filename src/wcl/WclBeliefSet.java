package wcl;

import java.util.*;

import net.sf.tweety.commons.*;
import net.sf.tweety.logics.cl.syntax.*;
import net.sf.tweety.logics.pl.syntax.*;

/**
 * This class models a belief set on conditional logic, i.e. a set of conditionals.
 *
 * @author Matthias Thimm
 *
 */
public class WclBeliefSet extends BeliefSet<WeakConditional> {

	/**
	 * Creates a new (empty) conditional belief set.
	 */
	public WclBeliefSet(){
		super();
	}

	/**
	 * Creates a new conditional belief set with the given collection of
	 * conditionals.
	 * @param conditionals a collection of conditionals.
	 */
	public WclBeliefSet(Collection<? extends WeakConditional> conditionals){
		super(conditionals);
	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.kr.BeliefBase#getSignature()
	 */
	@Override
	public PropositionalSignature getSignature(){
		PropositionalSignature sig = new PropositionalSignature();
		for(Formula f: this){
			WeakConditional c = (WeakConditional) f;
			sig.addAll(c.getPremise().getAtoms());
			sig.addAll(c.getConclusion().getAtoms());
		}
		return sig;
	}

	@Override
	public WclBeliefSet clone(){
		WclBeliefSet copy = new WclBeliefSet();
		Iterator<WeakConditional> i = this.iterator();
		while(i.hasNext()){
			copy.add(i.next());
		}
		return copy;
	}
}

