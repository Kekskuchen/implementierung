package wcl;
import java.util.Collection;
import java.util.HashSet;

import net.sf.tweety.commons.Formula;
import net.sf.tweety.commons.Signature;
import net.sf.tweety.commons.util.rules.Rule;
import net.sf.tweety.logics.cl.syntax.Conditional;
import net.sf.tweety.logics.commons.syntax.Predicate;
import net.sf.tweety.logics.commons.syntax.interfaces.Conjuctable;
import net.sf.tweety.logics.commons.syntax.interfaces.Disjunctable;
import net.sf.tweety.logics.commons.syntax.interfaces.SimpleLogicalFormula;
import net.sf.tweety.logics.fol.syntax.Disjunction;
import net.sf.tweety.logics.pl.syntax.Conjunction;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalFormula;

/**
 * This class represents weak conditionals (|B|A|) with formulas A,B.
 * @author Mandy Niolaus
 */
public class WeakConditional implements Formula {

	/**
	 * The premise of this conditional.
	 */
	private PropositionalFormula premise;

	/**
	 * The conclusion of this conditional.
	 */
	private PropositionalFormula conclusion;

	/**
	 * Creates a new weak conditional with the given premise
	 * and conclusion.
	 * @param premise the premise (a formula) of this conditional.
	 * @param conclusion the conclusion (a formula) of this conditional.
	 */
	public WeakConditional(PropositionalFormula premise, PropositionalFormula conclusion){
		this.premise = premise;
		this.conclusion = conclusion;
	}


	public PropositionalFormula getPremise(){
		return this.premise;
	}

	public PropositionalFormula getConclusion(){
		return this.conclusion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return "(|" + conclusion + "|" + premise + "|)";
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((conclusion == null) ? 0 : conclusion.hashCode());
		result = prime * result + ((premise == null) ? 0 : premise.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeakConditional other = (WeakConditional) obj;
		if (conclusion == null) {
			if (other.conclusion != null)
				return false;
		} else if (!conclusion.equals(other.conclusion))
			return false;
		if (premise == null) {
			if (other.premise != null)
				return false;
		} else if (!premise.equals(other.premise))
			return false;
		return true;
	}



	public void setConclusion(PropositionalFormula conclusion) {
		if(conclusion == null)
			throw new IllegalArgumentException();
		this.conclusion = conclusion;
	}


	public void setPremise(PropositionalFormula premise) {
		if(premise == null)
			throw new IllegalArgumentException();
		this.premise = premise;
	}
	public WeakConditional clone() {
		return new WeakConditional(premise.clone(), conclusion.clone());
	}

	/* (non-Javadoc)
	 * @see net.sf.tweety.kr.Formula#getSignature()
	 */
	@Override
	public Signature getSignature(){
		return this.premise.combineWithAnd(this.conclusion).getSignature();
	}
}
