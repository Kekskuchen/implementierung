package wcl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.sf.tweety.commons.Answer;
import net.sf.tweety.commons.Formula;
import net.sf.tweety.commons.Reasoner;
import net.sf.tweety.logics.cl.ClBeliefSet;
import net.sf.tweety.logics.cl.semantics.RankingFunction;
import net.sf.tweety.logics.cl.syntax.Conditional;
import net.sf.tweety.logics.pl.semantics.PossibleWorld;
import net.sf.tweety.logics.pl.syntax.PropositionalFormula;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;

public class ZWClReasoner extends Reasoner{

	private CombinedBeliefSet mybeliefbase;
	private RankingFunktionWithWeakConditionals crepresentation;
	private Map<Conditional, Integer> posPartitionranks;
	private Map<WeakConditional, Integer> negPartitionranks;
	private Set<PossibleWorld> omega = null;

	/**
	 * creates a new Reasoner for the given beliefbase
	 * @param beliefBase Knowledgebase
	 */
	public ZWClReasoner(CombinedBeliefSet beliefBase) {
		super(beliefBase.getPositiveBeliefSet());
		mybeliefbase = beliefBase.clone();
		this.omega = PossibleWorld.getAllPossibleWorlds( (PropositionalSignature) mybeliefbase.getSignature() );
		posPartitionranks = new HashMap<Conditional, Integer>();
		negPartitionranks = new HashMap<WeakConditional, Integer>();
	}

	@Override
	public Answer query(Formula query) {
		if(!(query instanceof Conditional) && !(query instanceof PropositionalFormula) && !(query instanceof WeakConditional))
			throw new IllegalArgumentException("Reasoning in conditional logic is only defined for conditional and propositional queries.");
		RankingFunktionWithWeakConditionals crepresentation = this.getCRepresentation();
		if(query instanceof Conditional || query instanceof WeakConditional){
			Answer answer = new Answer(this.getKnowledgeBase(),query);
			boolean bAnswer = crepresentation.satisfies(query);
			answer.setAnswer(bAnswer);
			if(query instanceof Conditional){
				System.out.println("haha");
				Conditional  act = (Conditional) query;
				PropositionalFormula conclusion =act.getConclusion();
				Iterator<PropositionalFormula> set =act.getPremise().iterator();
				PropositionalFormula premise = set.next();
				int rankVerificated = crepresentation.rank((PropositionalFormula)premise.combineWithAnd(conclusion));
				int rankFalsified = crepresentation.rank((PropositionalFormula)premise.combineWithAnd(conclusion.complement()));

				//rank infinite
				if(rankVerificated == Integer.MAX_VALUE){
					answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion)).toString()+ ") = \u221E" +   "\n");
				}
				else{
				answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion)).toString()+ ") = " + rankVerificated + "\n");
				}
				if(rankFalsified == Integer.MAX_VALUE){
					answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion.complement())).toString()+ ") = \u221E" +   "\n");
				}
				else{
				answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion.complement())).toString()+ ") = " + rankFalsified + "\n");
				}
			}
			if(query instanceof WeakConditional){
				System.out.println("huhu");
				WeakConditional  act = (WeakConditional) query;
				PropositionalFormula conclusion =act.getConclusion();
				PropositionalFormula premise = act.getPremise();
				int rankVerificated = crepresentation.rank((PropositionalFormula)premise.combineWithAnd(conclusion));
				int rankFalsified = crepresentation.rank((PropositionalFormula)premise.combineWithAnd(conclusion.complement()));
				//rank infinite
				if(rankVerificated == Integer.MAX_VALUE){
					answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion)).toString()+ ") = \u221E" +   "\n");
				}
				else{
				answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion)).toString()+ ") = " + rankVerificated + "\n");
				}
				if(rankFalsified == Integer.MAX_VALUE){
					answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion.complement())).toString()+ ") = \u221E" +   "\n");
				}
				else{
				answer.appendText("k(" + ((PropositionalFormula) premise.combineWithAnd(conclusion.complement())).toString()+ ") = " + rankFalsified + "\n");
				}
			}
			answer.appendText( "The answer is: " + bAnswer);
			return answer;
		}
		if(query instanceof PropositionalFormula){
			int rank = crepresentation.rank(query);
			Answer answer = new Answer(this.getKnowledgeBase(),query);
			answer.setAnswer(rank==0);
			if(rank != Integer.MAX_VALUE)
			answer.appendText("The rank of the query is " + rank + " (the query is " + ((rank==0)?(""):("not ")) + "believed)");
			else
				answer.appendText("The rank of the query is \u221E"  + " (the query is " + ((rank==0)?(""):("not ")) + "believed)");

			return answer;
		}

		return null;
	}

	/**
	 * returns a c-representation for the belief base
	 * @return {@link RankingFunktionWithWeakConditionals} C-Representation with pos. and neg. conditionals
	 */
	private RankingFunktionWithWeakConditionals getCRepresentation() {
		if(this.crepresentation == null)
			this.crepresentation = this.computeCRepresentation();
		return this.crepresentation;
	}

	/**
	 * computes a c-representation
	 * @return {@link RankingFunktionWithWeakConditionals} C-Representation with pos. and neg. conditionals
	 */
	public RankingFunktionWithWeakConditionals computeCRepresentation() {
		if(mybeliefbase.getPositiveBeliefSet().isEmpty() && mybeliefbase.getNegativeBeliefSet().isEmpty())
			return new RankingFunktionWithWeakConditionals(mybeliefbase.getSignature());

		CombinedBeliefSet toTolerate = mybeliefbase.clone();
		ArrayList<CombinedBeliefSet> partition = this.partition(mybeliefbase);
		RankingFunktionWithWeakConditionals crep = new RankingFunktionWithWeakConditionals(mybeliefbase.getSignature());
		CombinedBeliefSet actualToCompare =  partition.get(0);
		//set ranks of Conditionals in R_0 to 1
		for (Conditional cond : actualToCompare.getPositiveBeliefSet()) {
			posPartitionranks.put(cond, 1);
			toTolerate.remove(cond);
		}
		for (WeakConditional cond : actualToCompare.getNegativeBeliefSet()) {
			negPartitionranks.put(cond, 1);
			toTolerate.remove(cond);
		}
		partition.remove(0);
		//iterate over the partition
		for (CombinedBeliefSet combinedBeliefSet : partition) {
			for (Conditional cond : combinedBeliefSet.getPositiveBeliefSet()) {
				int rank = minToFalsify(cond, actualToCompare, omega, toTolerate);
				posPartitionranks.put(cond, rank+1);
			}
			for (WeakConditional cond : combinedBeliefSet.getNegativeBeliefSet()) {
				int rank = weakMinToFalsify(cond, actualToCompare, omega, toTolerate);
				System.out.println("Konditional: " + cond.toString() + ", Rang: " + rank);
				negPartitionranks.put(cond, rank);
			}
			actualToCompare.add(combinedBeliefSet);
			for (Conditional cond : combinedBeliefSet.getPositiveBeliefSet()) {
				toTolerate.remove(cond);
			}
			for (WeakConditional cond : combinedBeliefSet.getNegativeBeliefSet()) {
				toTolerate.remove(cond);
			}
		}
		mybeliefbase = actualToCompare;
		System.out.println(mybeliefbase.toString());
		System.out.println(posPartitionranks.toString());
		System.out.println(negPartitionranks.toString());

		//compute rang of the worlds
		for (PossibleWorld possibleWorld : omega) {
			int rank = 0;
			for ( Conditional cond : mybeliefbase.getPositiveBeliefSet()) {
				if(RankingFunction.falsifies(possibleWorld, cond)){
					rank = rank + posPartitionranks.get(cond);
				}
			}
			for ( WeakConditional cond : mybeliefbase.getNegativeBeliefSet()) {
				if(RankingFunktionWithWeakConditionals.falsifies(possibleWorld, cond)){
					rank = rank + negPartitionranks.get(cond);
				}
			}
			crep.setRank(possibleWorld, rank);
		}

		return crep;
	}

	private int minToFalsify(Conditional cond, CombinedBeliefSet actualToCompare, Set<PossibleWorld> omega2, CombinedBeliefSet toTolerate) {
		int min = Integer.MAX_VALUE;
		//System.out.println(cond.toString());
		//System.out.println("toCompare: "+ actualToCompare.toString());
		//System.out.println("toTolerate: " + toTolerate.toString());
		for (PossibleWorld possibleWorld : omega2) {
			int i = 0;
			if(RankingFunction.verifies(possibleWorld, cond) && isTolerated(toTolerate, possibleWorld)){

				for ( Conditional act : actualToCompare.getPositiveBeliefSet()) {
					if(RankingFunction.falsifies(possibleWorld, act)){
						//System.out.println( "Welt: " + possibleWorld + ", Kond: " + act.toString());
						i = i +  posPartitionranks.get(act);
					}
				}
				for ( WeakConditional act : actualToCompare.getNegativeBeliefSet()) {
					if(RankingFunktionWithWeakConditionals.falsifies(possibleWorld, act))
						i = i +  negPartitionranks.get(act);
				}
				//System.out.println("Welt: " + possibleWorld + ", i:" + i);
				if( i < min)
					min = i;
			}
		}
		return min;
	}
	private int weakMinToFalsify(WeakConditional cond, CombinedBeliefSet actualToCompare, Set<PossibleWorld> omega2, CombinedBeliefSet toTolerate) {
		int min = Integer.MAX_VALUE;
		System.out.println();
		System.out.println(cond.toString());
		System.out.println("toCompare: "+ actualToCompare.toString());
		System.out.println("toTolerate: " + toTolerate.toString());
		for (PossibleWorld possibleWorld : omega2) {
			int i = 0;
			if(RankingFunktionWithWeakConditionals.verifies(possibleWorld, cond) && isTolerated(toTolerate, possibleWorld) ){
				for ( Conditional act : actualToCompare.getPositiveBeliefSet()) {
					if(RankingFunction.falsifies(possibleWorld, act))
						i = i +  posPartitionranks.get(act);
				}
				for ( WeakConditional act : actualToCompare.getNegativeBeliefSet()) {
					if(RankingFunktionWithWeakConditionals.falsifies(possibleWorld, act))
						i = i +  negPartitionranks.get(act);
				}
				System.out.println("Welt: " + possibleWorld + ", i: " + i);
				if( i < min)
					min = i;
			}
		}
		return min;
	}

	/**
	 * Returns a partitioning of a knowledge base into partitions containing all conditionals that
	 * tolerate the remaining set of conditionals of a knowledge base.
	 * @param Knowledge base that needs to be partitioned
	 * @return ArrayList containing consistent belief sets
	 */
	public ArrayList<CombinedBeliefSet> partition( CombinedBeliefSet kb ){

		// create empty set of belief bases for the partitioning
		ArrayList<CombinedBeliefSet> tolerancePartition = new ArrayList<CombinedBeliefSet>();
		// Copy knowledge base to a second set from which we can remove tolerated conditionals
		ClBeliefSet knowledgebase = kb.getPositiveBeliefSet().clone();
		WclBeliefSet negkb = kb.getNegativeBeliefSet().clone();

		while( !knowledgebase.isEmpty() || !negkb.isEmpty()){

			ClBeliefSet posPartition = new ClBeliefSet();
			WclBeliefSet negPartition = new WclBeliefSet();

			for( Conditional f: knowledgebase ){
				// if the current conditional is tolerated by the remaining set of conditionals in the
				// knowledge base, add it to the current partition
				if( isTolerated(f,knowledgebase, negkb) ) {
					posPartition.add( f );
				}
			}
			for( WeakConditional f: negkb ){
				// if the current conditional is tolerated by the remaining set of conditionals in the
				// knowledge base, add it to the current partition
				if( isTolerated(f,knowledgebase, negkb) ) {
					negPartition.add( f );
				}
			}

			 /*
			  * If you're not doing any progress here, the belief base isn't
			  * consistent and evaluation should be aborted.
			  */
			 if (posPartition.isEmpty() && negPartition.isEmpty())
			  return new ArrayList<CombinedBeliefSet>();

			// adding the partition with all tolerated conditionals to the partition set
			// and remove all conditionals of this partition from the remaining conditional set
			tolerancePartition.add( new CombinedBeliefSet(posPartition, negPartition) );

			for( Conditional f : posPartition ){
				knowledgebase.remove(f);
			}
			for( WeakConditional f : negPartition ){
				negkb.remove(f);
			}
		}

		System.out.println( tolerancePartition );

		return tolerancePartition;
	}

	/**
	 * Checks whether or not the given formula is tolerated by the knowledge base,
	 * i.e., there is a world omega that satisfies the formula and does not falsify each conditional (B|A)
	 * in the knowledge base (it satisfies the material implication A => B).
	 * @param Conditional f - formula that should be tolerated by the knowledge base
	 * @param ClBeliefSet kb - corresponding knowledge base
	 * @return true if the Conditional f is tolerated, false otherwise
	 */
	private boolean isTolerated( Conditional f, ClBeliefSet kb, WclBeliefSet wkb ) {

		boolean tolerated = true;

		// Test whether or not there is a world that satisfies formula f
		// and does not falsify the remaining conditionals of the knowledge base
		for( PossibleWorld world : this.omega ){
			tolerated = true;

			// Test whether or not the current world satisfies the formula f.
			// If it does not satisfy the formula f, all further tests can be aborted.
			if( !world.satisfies(f.getPremise()) || ( world.satisfies(f.getPremise()) && !world.satisfies(f.getConclusion()) ) ){
				tolerated = false;
				continue;
			}

			for( Conditional c : kb ){

				// Test whether the current world which satisfies the given formula f does also
				// satisfy the material implication of the current conditional c.
				if( world.satisfies(c.getPremise()) ){
					if( !world.satisfies(c.getConclusion()) ){
						tolerated = false;
						break;
					}
				}
			}
			for( WeakConditional c : wkb ){

				// Test whether the current world which satisfies the given formula f does also
				// satisfy the material implication of the current conditional c.
				if( world.satisfies(c.getPremise()) ){
					if( !world.satisfies(c.getConclusion()) ){
						tolerated = false;
						break;
					}
				}
			}

			// if the current world represents an interpretation under which the formula f is tolerated, yeay
			if( tolerated == true ){
				break;
			}
		}

		return tolerated;
	}

	/**
	 * Checks whether or not the given formula is tolerated by the knowledge base,
	 * i.e., there is a world omega that satisfies the formula and does not falsify each conditional (B|A)
	 * in the knowledge base (it satisfies the material implication A => B).
	 * @param Conditional f - formula that should be tolerated by the knowledge base
	 * @param ClBeliefSet kb - corresponding knowledge base
	 * @return true if the Conditional f is tolerated, false otherwise
	 */
	private boolean isTolerated( WeakConditional f, ClBeliefSet kb, WclBeliefSet wkb ) {

		boolean tolerated = true;

		// Test whether or not there is a world that satisfies formula f
		// and does not falsify the remaining conditionals of the knowledge base
		for( PossibleWorld world : this.omega ){
			tolerated = true;

			// Test whether or not the current world satisfies the formula f.
			// If it does not satisfy the formula f, all further tests can be aborted.
			if( !world.satisfies(f.getPremise()) || ( world.satisfies(f.getPremise()) && !world.satisfies(f.getConclusion()) ) ){
				tolerated = false;
				continue;
			}

			for( Conditional c : kb ){

				// Test whether the current world which satisfies the given formula f does also
				// satisfy the material implication of the current conditional c.
				if( world.satisfies(c.getPremise()) ){
					if( !world.satisfies(c.getConclusion()) ){
						tolerated = false;
						break;
					}
				}
			}
			for( WeakConditional c : wkb ){

				// Test whether the current world which satisfies the given formula f does also
				// satisfy the material implication of the current conditional c.
				if( world.satisfies(c.getPremise()) ){
					if( !world.satisfies(c.getConclusion()) ){
						tolerated = false;
						break;
					}
				}
			}

			// if the current world represents an interpretation under which the formula f is tolerated, yeay
			if( tolerated == true ){
				break;
			}
		}

		return tolerated;
	}

	private boolean isTolerated(CombinedBeliefSet kb, PossibleWorld w){
		for(Conditional cond: kb.getPositiveBeliefSet()){
			if(RankingFunktionWithWeakConditionals.falsifies(w, cond))
				return false;
		}
		for(WeakConditional cond: kb.getNegativeBeliefSet()){
			if(RankingFunktionWithWeakConditionals.falsifies(w, cond))
				return false;
		}
		return true;
	}

}
